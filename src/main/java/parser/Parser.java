package parser;

import expression.Expression;
import expression.NumberExpression;

import java.util.ArrayDeque;
import java.util.List;
import java.util.Stack;


public class Parser {
    private Stack<Expression> stack;

    public Parser() {
        this.stack = new Stack<Expression>();
    }

    public int parse(String str){
        String[] tokenList = str.split(" ");

        for (String symbol : tokenList) {
            if (!ParseUtil.isOperator(symbol)) {

                Expression numberExpression = new NumberExpression(symbol);
                stack.push(numberExpression);
                System.out.println(String.format("Pushed to stack: %d", numberExpression.interpret()));
            } else  if (ParseUtil.isOperator(symbol)) {

                Expression firstExpression = stack.pop();
                Expression secondExpression = stack.pop();
                System.out.println(String.format("Popped operands %d and %d",
                        firstExpression.interpret(), secondExpression.interpret()));
                Expression operator = ParseUtil.getExpressionObject(firstExpression, secondExpression, symbol);
                System.out.println(String.format("Applying Operator: %s", operator));
                int result = operator.interpret();
                NumberExpression resultExpression = new NumberExpression(result);
                stack.push(resultExpression);
                System.out.println(String.format("Pushed result to stack: %d", resultExpression.interpret()));
            }
        }

        return stack.pop().interpret();
    }

}
