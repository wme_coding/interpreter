package expression;

public interface Expression {
    int interpret();
}
