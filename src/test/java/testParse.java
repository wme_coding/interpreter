import org.junit.Assert;
import org.junit.Test;
import parser.Parser;

public class testParse {
    @Test
    public void testParser(){
        String input = "5 6 5 + *";
        Parser expressionParser = new Parser();
        int result = expressionParser.parse(input);
        Assert.assertEquals(55, result);
    }
}
